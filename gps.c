#include <stdio.h>
#include <stdlib.h>

typedef struct gps
{
    float lat;
    float lon;
} gps;

void printgps(gps g);

gps * loadGPS(int *size)
{
    // Array of gps
    int arrayLength = 4;
    gps * arr = malloc(arrayLength * sizeof(gps));
    
    FILE *in = fopen("gps.txt", "r");
    if (!in)
    {
        perror("Can't open gps file");
        exit(1);
    }
    
    char line[50];
    int entries = 0;
    while (fgets(line, 50, in) != NULL)
    {
        if (entries == arrayLength)
        {
            arrayLength += 4;
            arr = realloc(arr, arrayLength * sizeof(gps));
        }
        
        sscanf(line, "%f%f", &arr[entries].lat, &arr[entries].lon);

        entries++;
    }
    
    *size = entries;
    
    return arr;
}


int main()
{
    int length;
    gps * coords = loadGPS(&length);
    
    for (int i = 0; i < length; i++)
    {
        printgps(coords[i]);
    }
}

void printgps(gps g)
{
    printf("(%f, %f)\n", g.lat, g.lon);
}