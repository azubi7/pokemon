#include <stdio.h>

struct gps 
{
    float lat;
    float lon;
};
typedef struct gps gps;

void printgps(gps g);

int main()
{
    // Set up an array of 10 GPS coordinates
    gps coords[10];
    
    // Add some coordinates
    coords[0].lat = 38.6;
    coords[0].lon = -121.5;
    
    coords[1].lat = 12.9;
    coords[1].lon = 52.5;
    
    gps here;
    here.lat = 19.4;
    here.lon = -45.8;
    
    coords[2] = here;
    
    printgps(coords[0]);
    printgps(coords[1]);
    printgps(coords[2]);
}

void printgps(gps g)
{
    printf("(%f, %f)\n", g.lat, g.lon);
}